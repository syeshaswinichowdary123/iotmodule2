# Basic Electronics for Embedded

## 1.Sensors and Actuators
> A transducer is any physical device that converts one form of energy into another.

**Sensors**
- Sensor is a transducer that converts any natural/physical signal into electrical signal.
Example:Microphone which takes voice signal as input and gives electric signal as output.

- There are many sensors used for particular purposes.
![sensor](extras/m1.png)

**Actuators**

- Actuator is another type of transducer that converts electric signal into physical signal.

** Different Types of Actuators**

![actuator](extras/m2.png)

## 2.Analog and Digital Signals

> Signal is a physical quantity that carries information and is dependent on independent parameters(time,space,distance etc.,).

### Analog Vs Digital

**Analog Signals**

- It is a continous signal where both amplitude and time is continuous.
- These are denoted by sine waves. And carries information using electric pulses.
>  Examples:Human voice in air, analog electronic devices.
- Analog technology records waveforms as they are.
> Applications:Thermometer

**Digital Signals**

- It is a discrete signal where amplitude is continuous and time is discrete.
- These are denoted by square waves. And carries information using binary digits.
> Examples:Computers, CDs, DVDs, and other digital electronic devices.
- digital signal is the result of analog signal where the values are taken at only discrete points.
> Applications:PCs, PDAs 

![signal](extras/m3.png)

## 3.Micro-controllers Vs Micro-processors

**Micro-processors**

- Micro-processor is a program controlled device that functions as cpu of the computer, which fetches the instructions from memory, decodes and execute the instructions.
> Examples: 8085,8086,80186,80286 ..., Micro-processors.
- It doesn't have internal memory like:RAM,ROM and other peripherals.
-It requires externals peripherals to perform certain tasks.

**Micro-controllers**

- Micro-controller is a chip which includes Micro-processors,memory,I/O ports all in one single chip.
> Example:8051 Micro-controller.
- It has internal memory such as RAM,ROM and other peripherals.
- It does not need any external circuits to do its tasks.

## 4.Introduction to RPi

![RPi](extras/m4.png)

- Raspberry Pi is a mini computer which is cost effective and can linux in it.
- It provides GPIO(general purpose input/output) pins and also posses large power for its size.
- We Can connect multiple Pi’s together. It can also be connected to sensors and actuators to collect data and perform operations.
- It is a SOC (System On Chip).We can connect shields (Shields - addon functionalities).
- Pi uses ARM(Acorn RISC Machine).
- **Raspberry Pi Interfaces:** GPIO,UART,SPI,I2C,PWM

## 5.Serial and Parallel Communication

![communication](extras/m5.png)

Let's consider serial and parallel communication in Digital communication.
- In parallel communication all the binary digits are sent at a time for processing.
***Parallel Interfaces:GPIO***
- Where as in serial communication binary digits are sent one after other.
***Serial Interfaces:UART,SPI,I2C***



